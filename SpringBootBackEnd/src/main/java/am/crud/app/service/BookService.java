package am.crud.app.service;

import am.crud.app.entity.Book;
import java.util.List;

public interface BookService {

   Long save(Book book);

   Book getOne(Long id);

   List<Book> findAll();

   void update(Long id, Book book);

   void delete(Long id);
}
