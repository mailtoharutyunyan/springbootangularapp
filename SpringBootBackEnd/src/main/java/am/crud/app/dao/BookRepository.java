package am.crud.app.dao;


import am.crud.app.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {

    Book save(Book book);

    Book getById(Long id);

    List<Book> findAll();

//    void update(Long id, Book book);

    void deleteById(Long id);

}
